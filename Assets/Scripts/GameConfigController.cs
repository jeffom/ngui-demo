﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// This would be better inside a game manager 
/// </summary>
public class GameConfigController : MonoBehaviour
{
    #region Singleton

    static GameConfigController m_gInstance;

    public static GameConfigController instance
    {
        get
        {
            return m_gInstance;
        }
    }

    #endregion

    /// <summary>
    /// Config Cached data
    /// </summary>
    Dictionary<string, object> m_dictConfig;        

    // Use this for initialization
	void Awake () {
        m_gInstance = this;

        var asset = Resources.Load("Data/data") as TextAsset;

	    //read the data file
        var decodedJson = MiniJSON.Json.Deserialize(asset.text) as Dictionary<string, object>;
        if (decodedJson != null)
        {
            var configs = decodedJson["Config"] as Dictionary<string, object>;
            if (configs != null)
                m_dictConfig = configs;
            else
                m_dictConfig = new Dictionary<string, object>();
            //Log("Loaded data successfully");
        }
	}
	
    /// <summary>
    /// Retrieves the "BiddingFeeSoft" config
    /// </summary>
    /// <returns></returns>
    public int GetBiddingFeeSoft()
    {
        var soft_bid = GetConfig("BiddingFeeSoft");
        if (soft_bid != null)
            return int.Parse(soft_bid.ToString());

        return -1;
    }

    /// <summary>
    /// Retrieves the "BiddingFeeSoft" config
    /// </summary>
    /// <returns></returns>
    public int GetBiddingFeePremium()
    {
        var prem_bid = GetConfig("BiddingFeePremium");
        if (prem_bid != null)
            return int.Parse(prem_bid.ToString());

        return -1;
    }


    object GetConfig(string sKey)
    {
        if (m_dictConfig.ContainsKey(sKey))
            return m_dictConfig[sKey];

        return null;
    }
        
    /// <summary>
    /// Parsing the role main attributes from the config
    /// </summary>
    /// <param name="eRole"></param>
    /// <returns></returns>
    AttributeType[] GetAttributes(FieldRole eRole)
    {
        List<AttributeType> returnList = new List<AttributeType>();

        Dictionary<string, object> roleMainAttributes = m_dictConfig["RoleMainAttributes"] as Dictionary<string,object>;
        if (roleMainAttributes != null && roleMainAttributes.ContainsKey(eRole.ToString()))
        {
            List<object> attributes = roleMainAttributes[eRole.ToString()] as List<object>;

            for(int i=0; i < attributes.Count; i++)
            {                
                AttributeType type = (AttributeType)System.Enum.Parse(typeof(AttributeType), attributes[i].ToString());
                returnList.Add(type);
            }
        }

        return returnList.ToArray();
    }

    void Log(string msg)
    {
        Debug.Log("[GameConfigController]" + msg);
    }

}
