﻿/// <summary>
/// Global Types (used for enums and structs)
/// </summary>

    public enum FieldRole
    {
        GK = 0,	    // goalkeeper

        LB = 1,	    // leftback
        CB = 2,	    // centralback
        RB = 3,	    // rightback

        DM = 4,	    // defensive middfield
        LM = 5,	    // left midfield
        CM = 6,	    // central midfield
        RM = 7,	    // right midfield

        LF = 13,	// left forward
        CF = 14,	// centralforward
        RF = 15,	// right forward

        NONE = -1   
    }

    public enum AttributeType
    {
        ATTACK = 0,
        DEFENSE,
        CONDITION,
        TACTIC,
        TECHNIQUE,
    }


