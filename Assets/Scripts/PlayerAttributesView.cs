﻿using UnityEngine;
using System.Collections;

public class PlayerAttributesView : MonoBehaviour
{
    [SerializeField]
    protected UILabel m_labelNameInfo;

    [SerializeField]
    protected UILabel m_labelAtkValue;

    [SerializeField]
    protected UILabel m_labelDefValue;

    [SerializeField]
    protected UILabel m_labelCondValue;

    [SerializeField]
    protected UILabel m_labelTactValue;

    [SerializeField]
    protected UILabel m_labelTechValue;


    protected void SetAttributes(PlayerAttributes attributes)
    {
        m_labelAtkValue.text = attributes.attack.ToString();
        m_labelDefValue.text = attributes.defense.ToString();
        m_labelCondValue.text = attributes.condition.ToString();
        m_labelTactValue.text = attributes.tactic.ToString();
        m_labelTechValue.text = attributes.technique.ToString();
    }

}
