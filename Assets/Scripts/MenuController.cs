﻿using UnityEngine;
using System.Collections;

public class MenuController : MonoBehaviour {

    #region Singleton

    static MenuController m_gInstance;

    public static MenuController instance
    {
        get
        {
            return m_gInstance;
        }
    }

    #endregion

    [SerializeField]
    PlayerDetailsView m_pProfileView;
    
    void Awake()
    {
        m_gInstance = this;
    }

    public void OpenProfile(PlayerData pData)
    {
        m_pProfileView.gameObject.SetActive(true);
        m_pProfileView.Load(pData);
    }

}
