﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

/// <summary>
/// Value type data structure
/// </summary>
public struct PlayerAttributes
{
    public int attack;
    public int defense;
    public int condition;
    public int tactic;
    public int technique;    
}

/// <summary>
/// Object Type Data Struct of a soccer player based on the JSON data
/// This is going to be our base Entity
/// </summary>
public class PlayerData
{
    int m_iId;       
    string m_sFirstName;
    string m_sLastName;
    int m_iJerseyNumber;
    string m_sJerseyColor;
    string m_sBirthday;
    string m_sNationality;
    int m_iWage;
    int m_iStarRating;    
    float m_fStamina;        
    float m_fMorale;
    FieldRole m_eMainRole;
    Dictionary<FieldRole, int> m_dictPlayerRoles;
    PlayerAttributes m_pAttributes;

    public PlayerData()
    {
        m_iId = -1;
        m_sFirstName = string.Empty;
        m_sLastName = string.Empty;
        m_iJerseyNumber = -1;
        m_sJerseyColor = string.Empty;
        m_sBirthday = string.Empty;
        m_sNationality  = string.Empty;
        m_iWage = 0;
        m_iStarRating  = 0;
        m_fStamina = 0;
        m_fMorale = 0;
        m_eMainRole = FieldRole.NONE; 
        m_dictPlayerRoles = new Dictionary<FieldRole, int>();
    }

    #region property accessors
    public int Id
    {
        get { return m_iId; }
        set { m_iId = value; }
    }

    public string FirstName
    {
        get { return m_sFirstName; }       
    }
    
    public string LastName
    {
        get { return m_sLastName; }       
    }

    public int JerseyNumber
    {
        get { return m_iJerseyNumber; }       
    }

    public string JerseyColor
    {
        get { return m_sJerseyColor; }        
    }

    public string Birthday
    {
        get { return m_sBirthday; }       
    }
    
    public string Nationality
    {
        get { return m_sNationality; }       
    }

    public int Wage
    {
        get { return m_iWage; }        
    }

    public int StarRating
    {
        get { return m_iStarRating; }       
    }

    public float Stamina
    {
        get { return m_fStamina; }
        set { m_fStamina = value; }
    }

    public float Morale
    {
        get { return m_fMorale; }
        set { m_fMorale = value; }
    }

    public FieldRole MainRole
    {
        get { return m_eMainRole; }
        set { m_eMainRole = value; }
    }
    
    public Dictionary<FieldRole, int> DictPlayerRoles
    {
        get { return m_dictPlayerRoles; }        
    }

    #endregion
            
    public int GetAge()
    {
        try
        {            
            DateTime birthDate = DateTime.Parse(m_sBirthday);

            //this should give us the player's age            
            return ( DateTime.UtcNow - birthDate ).Days / 365;
        }
        catch(Exception e)
        {

            return 0;
        }
    }

    public float GetPremiumBidding()
    {
        return -1;
    }

    public float GetSoftBidding()
    {
        return -1;
    }

    public FieldRole[] GetLearnedRoles()
    {
        List<FieldRole> list = new List<FieldRole>();
        foreach (FieldRole role in m_dictPlayerRoles.Keys)
        {
            if (role != MainRole && !list.Contains(role))
            list.Add(role);
        }
        return list.ToArray();        
    }
        
    public PlayerAttributes GetCurrentAttributes()
    {
        return m_pAttributes;
    }

    /// <summary>
    /// Loads the MiniJSON output data from the decoded JSON
    /// </summary>
    /// <param name="decodedData"></param>
    /// <returns></returns>
    public bool LoadData(Dictionary<string,object> decodedData)
    {
        //expecting every data from the json array to follow the same format
        try
        {
            m_iId = int.Parse(decodedData["Id"].ToString());
            m_sFirstName = decodedData["FirstName"].ToString();
            m_sLastName = decodedData["LastName"].ToString();
            m_iJerseyNumber = int.Parse(decodedData["JerseyNumber"].ToString());
            m_sJerseyColor = decodedData["JerseyColor"].ToString();
            m_sBirthday = decodedData["BirthDay"].ToString();
            m_sNationality = decodedData["Nationality"].ToString();
            m_iWage = int.Parse(decodedData["Wage"].ToString());
            m_iStarRating = int.Parse(decodedData["StarRating"].ToString());
            m_fStamina = float.Parse(decodedData["Stamina"].ToString());
            m_fMorale = float.Parse(decodedData["Stamina"].ToString());
            m_eMainRole = (FieldRole)int.Parse(decodedData["MainRole"].ToString());

            //learned roles parsing
            var learned_roles = decodedData["LearnedRoles"] as List<object>;
            m_dictPlayerRoles = ParseLearnedRoles(learned_roles);

            var attributes = decodedData["Attributes"] as Dictionary<string, object>;
            m_pAttributes = ParseAttributes(attributes);

            return true;
        }
        catch(Exception e)
        {
            Debug.LogError("Couldnt load the data:" + e.StackTrace);
            return false;
        }
    }

    /// <summary>
    /// Parses through the array of learned roles
    /// </summary>
    /// <param name="roles"></param>
    private Dictionary<FieldRole, int> ParseLearnedRoles(List<object> roles)
    {
        Dictionary<FieldRole, int> retDict = new Dictionary<FieldRole, int>();

        if (roles != null)
        {
            for (int i = 0; i < roles.Count; i++)
            {
                Dictionary<string,object> role = roles[i] as Dictionary<string,object>;
                if(role != null)
                {
                    FieldRole roleEnum = (FieldRole)int.Parse(role["Role"].ToString());
                    int strength = int.Parse(role["Strength"].ToString());

                    //only the first of each will be set (no repeated)
                    if (!retDict.ContainsKey(roleEnum))
                        retDict.Add(roleEnum, strength);
                }                
            }            
        }

        return retDict;
    }

    private PlayerAttributes ParseAttributes(Dictionary<string,object> attributes)
    {
        PlayerAttributes retAttributes = new PlayerAttributes();
        retAttributes.attack = int.Parse(attributes["Attack"].ToString());
        retAttributes.defense = int.Parse(attributes["Defense"].ToString());
        retAttributes.condition = int.Parse(attributes["Condition"].ToString());
        retAttributes.tactic = int.Parse(attributes["Tactic"].ToString());
        retAttributes.technique = int.Parse(attributes["Technique"].ToString());

        return retAttributes;
    }

    /// <summary>
    /// Converts the data structure to a Json format
    /// </summary>
    /// <returns></returns>
    public string ToJSON()
    {
        throw new System.NotImplementedException();
    }

}
