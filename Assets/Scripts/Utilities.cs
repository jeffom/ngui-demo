﻿using UnityEngine;
using System.Collections;

public static class Utilities {

    /// <summary>
    /// Source http://answers.unity3d.com/questions/812240/convert-hex-int-to-colorcolor32.html
    /// Converts a Hex String in the format "0X00FF00"
    /// To Color data structure
    /// </summary>
    /// <param name="c"></param>
    /// <param name="sHexString"></param>
    public static Color HexToColor(string sHexString)
    {
         uint bigint = System.Convert.ToUInt32(sHexString, 16);
         var r = (bigint >> 16) & 255;
         var g = (bigint >> 8) & 255;
         var b = bigint & 255;

         return new Color(r,g,b);
    }

    /// <summary>
    /// Triest to create a new instance of a prefab object
    /// Only succeeds if we can get the component we want
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="referenceItem"></param>
    /// <returns></returns>
    public static T CreateInstance<T> (GameObject referenceItem)
        where T : MonoBehaviour
    {
        T retItem = null;

        if (referenceItem != null)
        {
            GameObject instance = (GameObject)GameObject.Instantiate(referenceItem);

            retItem = instance.GetComponent<T>();
            if (retItem != null)
            {                
                return retItem ;
            }
            else
            {
                GameObject.Destroy(instance);
            }
        }
        return null;
    }

    public static void Reparent(Transform pParent, Transform pNewChild)
    {
        if (pParent != null && pNewChild != null)
        {
            pNewChild.parent = pParent;
            pNewChild.localPosition = Vector3.zero;
            pNewChild.localRotation = Quaternion.identity;
            pNewChild.localScale = Vector3.one;
            pNewChild.gameObject.layer = pParent.gameObject.layer;
        }
    }

    public static void FillStarBars(int iStars, UIProgressBar background, UIProgressBar foreground)
    {
        foreground.value = 0;
        background.value = 0;

        while( iStars > 0 )
        {
            if( iStars % 2  == 0 )
                foreground.value += Consts.STAR_STEP;
            else
                background.value += Consts.STAR_STEP;            

            iStars--;
        }

        foreground.ForceUpdate();
        background.ForceUpdate();
    }

}
