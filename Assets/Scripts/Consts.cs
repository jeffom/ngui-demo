﻿using UnityEngine;
using System.Collections;

/// <summary>
/// constant values class
/// </summary>
public static class Consts {
    public const float STAR_STEP = 0.2f;    
}
