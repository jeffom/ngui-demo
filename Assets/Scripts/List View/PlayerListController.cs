﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerListController : MonoBehaviour {

    #region Singleton

    static PlayerListController m_gInstance;

    public static PlayerListController instance
    {
        get
        {
            return m_gInstance;
        }
    }

    #endregion

    [SerializeField]
    PlayersListView m_pListView;

    List<PlayerData> m_listPlayers;

    void Awake()
    {
        m_gInstance = this;
    }

	// Use this for initialization
	void Start () {        
        var asset = Resources.Load("Data/data") as TextAsset;
        m_listPlayers = ParseData(asset);
        SortList();        

        StartCoroutine( m_pListView.PopulateList(m_listPlayers.ToArray()) );
	}

    /// <summary>
    /// Parsing the mock data
    /// </summary>
    /// <param name="pData"></param>
    /// <returns></returns>
    List<PlayerData> ParseData(TextAsset pData)
    {        
        //initializing vars
        List<PlayerData> listPlayers = new List<PlayerData>();

        //read the data file
        var decodedJson = MiniJSON.Json.Deserialize(pData.text) as Dictionary<string, object>;
        if (decodedJson != null)
        {
            Log("Loaded data successfully");
            Log(decodedJson["Config"].ToString());

            var players = decodedJson["Players"] as List<object>;
            for (int i = 0; i < players.Count; i++)
            {
                var decodedPlayerData = players[i] as Dictionary<string, object>;
                if (decodedPlayerData != null)
                {
                    PlayerData newData = new PlayerData();
                    if (newData.LoadData(decodedPlayerData))
                    {
                        listPlayers.Add(newData);
                    }
                }
            }
        }
        else
        {
            Debug.LogError("Invalid data Set");
        }

        return listPlayers;
    }
    
    void SortList()
    {
        m_listPlayers.Sort( delegate(PlayerData p1, PlayerData p2) {
            return (p1.MainRole.CompareTo(p2.MainRole)); 
        });
    }

    void Log(string msg)
    {
        Debug.Log(msg);
    }

    /// <summary>
    /// Get the next one in the list order
    /// </summary>
    /// <param name="?"></param>
    /// <returns></returns>
    public PlayerData GetNext(PlayerData pData)
    {
        for(int i = 0; i <  m_listPlayers.Count ; i++)
        {
            //are one and the same?
            if( object.ReferenceEquals(pData, m_listPlayers[i]) )
            {
                if (i+1 < m_listPlayers.Count)
                    return m_listPlayers[i+1];
            }
        }

        return null;
    }

    /// <summary>
    /// Get the previous one from the list order
    /// </summary>
    /// <returns></returns>
    public PlayerData GetPrev(PlayerData pData)
    {
        for (int i = 0; i < m_listPlayers.Count; i++)
        {
            //are one and the same?
            if (object.ReferenceEquals(pData, m_listPlayers[i]))
            {
                if (i-1 >= 0)
                    return m_listPlayers[i-1];
            }
        }

        return null;
    }
}
