﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// This is the scroll list view
/// </summary>
public class PlayersListView : MonoBehaviour {

    /// <summary>
    /// Base objects to be instantiated in the scroll list
    /// </summary>
    [SerializeField]
    GameObject m_pItemReference;

    [SerializeField]
    UIScrollView m_scrollView;

    [SerializeField]
    UIGrid m_pGrid;

    List<PlayerListItemView> m_listInstances;
    
    void Awake()
    {
        m_listInstances = new List<PlayerListItemView>();
    }
        
    public IEnumerator PopulateList(PlayerData[] pDatas)
    {
        //clear the current view
        CleanList();
        yield return null;

        for (int i = 0; i < pDatas.Length; i++)
        {
            var newInstance = GetInstance();
            if (newInstance != null)
            {                
                newInstance.Load(pDatas[i]);
                AddItem(newInstance.transform);
            }
        }

        m_scrollView.BroadcastMessage("ParentHasChanged");
        m_pGrid.Reposition();
    }       

    void AddItem(Transform t)
    {
        //add the items    
        Utilities.Reparent(m_pGrid.transform, t);
    }


    bool NeedToCreateInstances(PlayerData[] pDatas)
    {
        return (pDatas.Length > m_listInstances.Count);
    }

    private void CleanList()
    {
        for (int i = 0; i < m_listInstances.Count; i++)
        {
            //resetting parent
            m_listInstances[i].transform.parent = this.transform;
            m_listInstances[i].gameObject.SetActive(false);
        }

        //resetting grid
        m_pGrid.GetChildList().Clear();

        m_scrollView.ResetPosition();
    }

    PlayerListItemView GetInstance()
    {
        for (int i = 0; i < m_listInstances.Count; i++ )
        {
            if (!m_listInstances[i].gameObject.activeInHierarchy)
                return m_listInstances[i];
        }

        var newInstance = Utilities.CreateInstance<PlayerListItemView>(m_pItemReference);
        m_listInstances.Add(newInstance);

        return newInstance;
    }
        

}
