﻿using UnityEngine;
using System.Collections;

/// <summary>
/// This is the class that'll help us set the values in the list item
/// </summary>
public class PlayerListItemView : PlayerAttributesView
{
    [SerializeField]
    UILabel m_labelNumber;

    [SerializeField]
    UILabel m_labelRole;
    
    [SerializeField]
    UILabel m_labelLearnedRoles;
    
    [SerializeField]
    UIProgressBar m_progressStamina;

    [SerializeField]
    UIProgressBar m_progressMorale;

    [SerializeField]
    UIProgressBar m_progressStarLevel1;

    [SerializeField]
    UIProgressBar m_progressStarLevel2;

    //Player data reference
    PlayerData m_pData;

    const float MAX_STARS_LEVEL = 1f;

    void Awake()
    {
        m_progressStarLevel1.value = 0;
        m_progressStarLevel2.value = 0;
        //m_progressStarLevel.value = 0; 
        
        //NGUITools.AddWidgetCollider(gameObject);
    }

	// Use this for initialization
	void Start () {
	
	}

    void CleanList()
    {
       
    }
	
    /// <summary>
    /// Updates the data in the view
    /// </summary>
    /// <param name="pData"></param>
    public void Load(PlayerData pData)
    {
        m_pData = pData;

        m_labelRole.text = pData.MainRole.ToString();
        m_labelNumber.text = pData.JerseyNumber.ToString();

        m_labelNameInfo.text = string.Format("{0} {1} ({2})", pData.FirstName, pData.LastName, pData.GetAge());

        string rolesStr = pData.MainRole.ToString();

        FieldRole[] learnedRoles = pData.GetLearnedRoles();
        if (learnedRoles.Length > 0)
        {
            rolesStr += " (";
            for (int i = 0; i < learnedRoles.Length; i++)
            {
                rolesStr += learnedRoles[i].ToString();
                rolesStr += ", ";
            }
            rolesStr = System.Text.RegularExpressions.Regex.Replace(rolesStr, ", $", "");   //removing unwanted comma         
            rolesStr += ")";
        }

        m_labelLearnedRoles.text = rolesStr;

        m_progressStamina.value = pData.Stamina;
        m_progressMorale.value = pData.Morale;
        
        //setting player star's
        SetStarsLevel(pData.StarRating);

        //now setting the attributes
        PlayerAttributes pAttributes = pData.GetCurrentAttributes();
        SetAttributes(pAttributes);

    }

    /// <summary>
    /// Sets the bar leves based on the stars level
    /// </summary>
    /// <param name="stars"></param>
    private void SetStarsLevel(int stars)
    {        
        m_progressStarLevel1.value = stars / 5.0f;

        if (stars <= 5)
        {
            m_progressStarLevel2.value = 0;
            m_progressStarLevel1.value = stars / 5.0f;
        }
        else
        {
            m_progressStarLevel1.value = MAX_STARS_LEVEL;
            m_progressStarLevel2.value = (stars % 5) / 5.0f;
        }

        m_progressStarLevel1.ForceUpdate();
        m_progressStarLevel2.ForceUpdate();

        Utilities.FillStarBars(stars, m_progressStarLevel1, m_progressStarLevel2);
    }
        
    public void OnClickData()
    {
        Log("OnClickData");

        //open the profile window for the player
        MenuController.instance.OpenProfile(m_pData);
    }

    void Log(string msg)
    {
        Debug.Log("[PlayerListItemView]" + msg);
    }
}
